<?php

/**
 * @file
 * Defines functionality for the Field Embed module.
 */

/**
 * Regular expression for the field embed parser.
 */
define('FIELD_EMBED_FILTER_EXPRESSION', '/\[\[(\w+)=(\d+):(\w+)(:\w+)?\]\]/');

/**
 * Implements hook_help().
 */
function field_embed_help($path, $arg = array()) {
  $output = '';
  switch ($path) {
    case 'admin/help#field_embed':
      $output .= '<p>' . t('The Field Embed module provides a filter that allows any field on a page to be embedded into another field.') . '</p>';
      break;

    default:
      break;
  }

  return $output;
}

/**
 * Implements hook_filter_info().
 */
function field_embed_filter_info() {
  return array(
    'field_embed' => array(
      'title' => t('Field embed'),
      'description' => t('By including the syntax [[entity_name=entity_id:(field machine name):(field view mode)]]], this filter will embed the field with a given field machine name in an entity, with an optional field formatter.'),
      'process callback' => 'field_embed_filter_field_embed_process',
      'tips callback' => 'field_embed_filter_field_embed_tips',
      'cache' => FALSE,
    ),
  );
}

/**
 * Filter process callback.
 *
 * @param string $text
 *   Return the callback to replace found filter items.
 *
 * @return string
 *   Callback for processing the matches.
 */
function field_embed_filter_field_embed_process($text) {
  // Return a regular expression match on the text.
  return preg_replace_callback(FIELD_EMBED_FILTER_EXPRESSION, '_field_embed_make_replacements', $text);
}

/**
 * Filter tips callback.
 *
 * @return string
 *   Returns helper text for the filter.
 */
function field_embed_filter_field_embed_tips() {
  // Get the entity object.
  $menu_item = menu_get_item(current_path());
  foreach ($menu_item['page_arguments'] as $argument) {
    if (isset($argument->field_embed_entity_type)) {
      $entity = $argument;
      break;
    }
  }

  $items = array();
  if (isset($entity)) {
    // Get the entity id.
    list($entity_id, $vid, $bundle) = entity_extract_ids($entity->field_embed_entity_type, $entity);

    if (isset($entity->field_embed_entity_type) && isset($entity->field_embed_bundle_name)) {
      $fields = field_info_instances($entity->field_embed_entity_type, $entity->field_embed_bundle_name);
      $formatter_types = field_info_formatter_types();

      // Get an item list for the fields.
      foreach ($fields as $field_name => $field) {
        $item = t('@name: [[!entity_type=!entity_id:!field_name]]',
          array(
            '@name' => $field['label'],
            '!entity_type' => $entity->field_embed_entity_type,
            '!entity_id' => $entity_id,
            '!field_name' => $field_name,
          )
        );
        $modes = array();
        if ($field_info = field_info_field($field['field_name'])) {
          $field_type = $field_info['type'];
          $formatters = _field_embed_get_formatters_by_type($field_type, $formatter_types);

          foreach ($formatters as $formatter_name => $formatter) {
            $modes[] = t('@label @description : [[!entity_type=!entity_id:!field_name:!formatter]]', array(
              '!entity_type' => $entity->field_embed_entity_type,
              '!entity_id' => $entity_id,
              '!field_name' => $field_name,
              '!formatter' => $formatter_name,
              '@label' => $formatter['label'],
              '@description' => ($formatter['description']) ? '(' . $formatter['description'] . ')' : '',
            ));
          }

          if (count($modes) > 1) {
            $item .= theme('item_list', array(
              'items' => $modes,
              'title' => NULL,
              'type' => 'ul',
              'attributes' => array(),
            ));
          }
        }

        $items[] .= $item;
      }
    }
  }

  // Return some filtered tips.
  $output = t('Fields are embedded into other fields by using the syntax
    [[entity_type=entity_id:field_machine_name(:field_formatter)]]. Field
    formatter is optional; leaving this blank will use the default field
    formatter for the field type.');

  // If we have items, attach those to the string.
  if (!empty($items)) {
    $output .= t('You may embed the following fields below:');
    $output .= theme('item_list', array(
      'items' => $items,
      'title' => NULL,
      'type' => 'ul',
      'attributes' => array(),
    ));
  }

  return $output;
}

/**
 * Callback that handles replacement of fields on entity mode.
 *
 * @param array $matches
 *   String matches for preg_replace callback.
 *
 * @return mixed
 *   Returns a rendered field if one matches the embed code, or the original
 *   content.
 *   If no matches, returns FALSE.
 *
 * @see field_embed_filter_field_embed_process()
 */
function _field_embed_make_replacements(array $matches) {
  // Get the field name and default in an easy to use format.
  $match = (isset($matches[0])) ? $matches[0] : NULL;
  $entity_type = (isset($matches[1])) ? $matches[1] : NULL;
  $entity_id = (isset($matches[2])) ? $matches[2] : NULL;
  $field_name = (isset($matches[3])) ? $matches[3] : NULL;
  $formatter = (isset($matches[4])) ? str_replace(':', '', $matches[4]) : NULL;

  // Load the entity. If entity is not set at this point, return the original.
  if (!$entity = entity_load($entity_type, array($entity_id))) {
    return $original;
  }

  // Set the entity.
  $entity = $entity[$entity_id];

  // Grab some variable information.
  $entity_type = $entity->field_embed_entity_type;
  $bundle_name = $entity->field_embed_bundle_name;
  $langcode = (isset($entity->language)) ? $entity->language : LANGUAGE_NONE;

  // Start with a blank field.
  $output = '';

  // Get the field instance.
  if (field_get_items($entity_type, $entity, $field_name) && $field_info = field_info_field($field_name)) {
    $formatter_types = field_info_formatter_types();
    $field_type = $field_info['type'];
    $formatters = _field_embed_get_formatters_by_type($field_type, $formatter_types);

    // Check the formatter. If it's blank, get the default formatter for the
    // widget type.
    if (empty($formatter)) {
      $formatter = array_keys($formatters);
      $formatter = $formatter[0];
    }

    // Get the module.
    $module = $formatters[$formatter]['module'];

    // Get the items we need to invoke formatter_view.
    $instance = field_info_instance($entity_type, $field_name, $bundle_name);
    $items = field_get_items($entity_type, $entity, $field_name, $langcode);

    // Get the display.
    $display = field_get_display($instance, 'default', $entity);
    $display['type'] = $formatter;

    // Prepare the instance with our display.
    unset($instance['display']);
    $instance['display']['default'] = $display;

    // Prepare the attach.
    list($id, ,) = entity_extract_ids($entity_type, $entity);
    $entities = array($id => $entity);
    $instances = array($id => $entity);
    $items = array($id => $items);

    // Make the attachment. NOTE: because of the way module_invoke works, we
    // cannot directly invoke a function that requires a parameter by reference.
    // We need to check to see if the module and hook exist, then directly call
    // the function.
    if (module_hook($module, 'field_prepare_view')) {
      $function = $module . '_field_prepare_view';
      $function($entity_type, $entities, $field_info, $instances, $langcode, $items);
    }
    if (module_hook($module, 'field_formatter_prepare_view')) {
      $function = $module . '_field_formatter_prepare_view';
      $function($entity_type, $entities, $field_info, $instances, $langcode, $items, array($id => $display));
    }

    // $items now has objects ready to render. Reassign it back to entity, and
    // then call field_view_value on the entity (since we want to respect other
    // settings that the user may have made in 'Manage Displays').
    $entity->{$field_name}[$langcode] = $items[$id];
    $view = field_view_field($entity_type, $entity, $field_name, $display, $langcode);
    $view['#theme'] = 'field_embed';

    $output = render($view);
  }

  return $output;
}

/**
 * Implements hook_theme().
 */
function field_embed_theme() {
  return array(
    'field_embed' => array(
      'render element' => 'element',
    ),
  );
}

/**
 * Theme preprocess function for theme_field_embed() and field-embed.tpl.php.
 *
 * @param array $variables
 *   Variables passed to the theme function.
 *
 * @see theme_field_embed()
 * @see field-embed.tpl.php
 */
function template_preprocess_field_embed(array &$variables) {
  $element = $variables['element'];

  // There's some overhead in calling check_plain() so only call it if the label
  // variable is being displayed. Otherwise, set it to NULL to avoid PHP
  // warnings if a theme implementation accesses the variable even when it's
  // supposed to be hidden. If a theme implementation needs to print a hidden
  // label, it needs to supply a preprocess function that sets it to the
  // sanitized element title or whatever else is wanted in its place.
  $variables['label_hidden'] = ($element['#label_display'] == 'hidden');
  $variables['label'] = $variables['label_hidden'] ? NULL : check_plain($element['#title']);

  // We want other preprocess functions and the theme implementation to have
  // fast access to the field item render arrays. The item render array keys
  // (deltas) should always be a subset of the keys in #items, and looping on
  // those keys is faster than calling element_children() or looping on all keys
  // within $element, since that requires traversal of all element properties.
  $variables['items'] = array();
  foreach ($element['#items'] as $delta => $item) {
    if (!empty($element[$delta])) {
      $variables['items'][$delta] = $element[$delta];
    }
  }

  // Add default CSS classes. Since there can be many fields rendered on a page,
  // save some overhead by calling strtr() directly instead of
  // drupal_html_class().
  $variables['field_name_css'] = strtr($element['#field_name'], '_', '-');
  $variables['field_type_css'] = strtr($element['#field_type'], '_', '-');
  $variables['classes_array'] = array(
    'field',
    'field-embed',
    'field-name-' . $variables['field_name_css'],
    'field-type-' . $variables['field_type_css'],
    'field-label-' . $element['#label_display'],
  );
  // Add a "clearfix" class to the wrapper since we float the label and the
  // field items in field.css if the label is inline.
  if ($element['#label_display'] == 'inline') {
    $variables['classes_array'][] = 'clearfix';
  }

  // Add specific suggestions that can override the default implementation.
  $variables['theme_hook_suggestions'] = array(
    'field_embed',
    'field_embed__' . $element['#field_type'],
    'field_embed__' . $element['#field_name'],
    'field_embed__' . $element['#bundle'],
    'field_embed__' . $element['#field_name'] . '__' . $element['#bundle'],
  );
}

/**
 * Theme process function for theme_field_embed() and field-embed.tpl.php.
 *
 * @param array $variables
 *   Variables passed to the theme function.
 *
 * @see theme_field_embed()
 * @see field-embed.tpl.php
 */
function template_process_field_embed(array &$variables) {
  // The default theme implementation is a function, so template_process() does
  // not automatically run, so we need to flatten the classes and attributes
  // here. For best performance, only call drupal_attributes() when needed, and
  // note that template_preprocess_field() does not initialize the
  // *_attributes_array variables.
  $variables['classes'] = (isset($variables['classes_array'])) ? implode(' ', $variables['classes_array']) : '';
  $variables['attributes'] = empty($variables['attributes_array']) ? '' : drupal_attributes($variables['attributes_array']);
  $variables['title_attributes'] = empty($variables['title_attributes_array']) ? '' : drupal_attributes($variables['title_attributes_array']);
  $variables['content_attributes'] = empty($variables['content_attributes_array']) ? '' : drupal_attributes($variables['content_attributes_array']);
  foreach ($variables['items'] as $delta => $item) {
    $variables['item_attributes'][$delta] = empty($variables['item_attributes_array'][$delta]) ? '' : drupal_attributes($variables['item_attributes_array'][$delta]);
  }
}

/**
 * Returns HTML for a field embed.
 *
 * @param array $variables
 *   Variables passed to the theme function.
 *
 * @return array
 *   Render array representing the field_embed field.
 *
 * @see template_preprocess_field_embed()
 * @see template_process_field_embed()
 * @see field-embed.tpl.php
 *
 * @ingroup themeable
 */
function theme_field_embed(array $variables) {
  // Render the top-level DIV.
  $output = array(
    '#prefix' => '<div class="' . $variables['classes'] . '">',
    '#suffix' => '</div>',
  );

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output['label'] = array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array('field-label'),
      ),
      'value' => array(
        '#markup' => $variables['label'] . ':',
      ),
    );
    if (isset($variables['title_attributes']) && is_array($variables['title_attributes'])) {
      $output['label']['#attributes'] += $variables['title_attributes'];
    }
  }

  // Render the items.
  $output['items'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('field-items'),
    ),
  );
  if (isset($variables['content_attributes']) && is_array($variables['content_attributes'])) {
    $output['items']['#attributes'] += $variables['content_attributes'];
  }
  foreach ($variables['items'] as $delta => $item) {
    $class = ($delta % 2 ? 'odd' : 'even');
    $output['items'][$delta] = array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array('field-item', $class),
      ),
      'value' => array(
        '#markup' => drupal_render($item),
      ),
    );
    if (isset($variables['item_attributes'][$delta]) && is_array($variables['item_attributes'][$delta])) {
      $output['items'][$delta]['#attributes'] += $variables['item_attributes'][$delta];
    }
  }

  return render($output);
}

/**
 * Implements hook_entity_load().
 *
 * Attaches entity_type and bundle_name to all entities.
 */
function field_embed_entity_load(&$entities, $type) {
  // Attach entity_type and bundle_name to all entities so our validate handler
  // has a consistent way to validate our field embeds.
  foreach ($entities as &$entity) {
    field_embed_entity_attach_properties($entity, $type);
  }
}

/**
 * Implements hook_entity_presave().
 */
function field_embed_entity_presave($entity, $type) {
  // Make sure all new content types have an entity_type and bundle_name for
  // use in validation.
  field_embed_entity_attach_properties($entity, $type);
}

/**
 * Helper function to attach new properties to all entities.
 */
function field_embed_entity_attach_properties(&$entity, $type) {
  // Attach the entity_type property.
  if (!isset($entity->field_embed_entity_type)) {
    $entity->field_embed_entity_type = $type;
  }

  // Attach a bundle_name property.
  $ids = entity_extract_ids($type, $entity);
  if (!isset($entity->field_embed_bundle_name)) {
    // Key of 2 has the bundle name, or NULL.
    $entity->field_embed_bundle_name = $ids[2];
  }
}

/**
 * Implements hook_field_attach_form().
 */
function field_embed_field_attach_form($entity_type, $entity, &$form, &$form_state, $langcode) {
  // Attach the entity and entity_type to the form_state array to access later.
  $form_state['entity_type'] = $entity_type;
  $form_state['entity'] = $entity;

  // Attach properties to our entity object.
  field_embed_entity_attach_properties($form_state['entity'], $form_state['entity_type']);

  // Attach a new validator before any default validators.
  $validate = (isset($form['#validate'])) ? $form['#validate'] : array();
  $form['#validate'] = array_merge(array('field_embed_field_form_validate'), $validate);
}

/**
 * Validation callback for the field_embed filter.
 *
 * @param array $form
 *   The form structure where field elements are attached to. This might be a
 *   full form structure, or a sub-element of a larger form.
 * @param array $form_state
 *   An associative array containing the current state of the form.
 */
function field_embed_field_form_validate(array $form, array &$form_state) {
  // Get the entity.
  if (isset($form_state['entity']) && isset($form_state['field'])) {
    // Get the entity into an easy to use variable.
    $entity = $form_state['entity'];
    $fields = field_info_instances($entity->field_embed_entity_type, $entity->field_embed_bundle_name);
    $fields = array_keys($fields);

    // Get a helper array of just the field elements on the page.
    $entity_fields = array();
    foreach (element_children($form) as $child) {
      if (in_array($child, $fields) && _field_embed_element_children_has_format($form[$child], $child)) {
        $entity_fields[$child] = &$form[$child];
      }
    }

    // Loop through the field elements on the page, looking for formats.
    foreach ($entity_fields as $field_name => $field) {
      $values = drupal_array_get_nested_value($form_state['values'], $field['#array_parents']);
      if (_field_embed_value_check_field($values, $field_name)) {
        form_set_error(implode('][', $form[$field_name]['#parents']), t('Circular reference detected.'));
      }
    }
  }
}

/**
 * Helper function to find all formatters for a given type.
 *
 * @param string $field_type
 *   A field type to find formatters for.
 * @param array $formatters
 *   An array of field formatters.
 *
 * @return array
 *   An array of field formatters for a given type.
 */
function _field_embed_get_formatters_by_type($field_type, $formatters = array()) {
  // Check to see if $formatters is set. If it's not, set it to all formatters
  // as a failsafe.
  if (!$formatters) {
    $formatters = field_info_formatter_types();
  }

  // Create the return array.
  $return = array();

  // Loop through the formatters, looking for the field type. If it exists,
  // add it to the return array with a label and description so other parts of
  // the module can use it.
  foreach ($formatters as $key => $formatter) {
    if (isset($formatter['field types']) && is_array($formatter['field types']) && in_array($field_type, $formatter['field types'])) {
      $return[$key] = array(
        'label' => $formatter['label'],
        'description' => (isset($formatter['description'])) ? $formatter['description'] : FALSE,
        'module' => $formatter['module'],
      );
    }
  }

  return $return;
}

/**
 * Helper function to recursively find all elements with a filter format.
 *
 * @param array $element
 *   A Form API element.
 * @param string $field_name
 *   The name of the field.
 *
 * @return bool
 *   Returns TRUE if the element has a #format parameter nested within it, and
 *   FALSE if the element does not.
 */
function _field_embed_element_children_has_format(array &$element, $field_name) {
  // Loop through all of the element children to see if any contain a #format
  // key. If so, we count them as format-ready to check for validation.
  foreach (element_children($element) as $key) {
    $child = $element[$key];

    // If the child has a #format parameter, this field has a formatter.
    if (isset($child['#format'])) {
      return $field_name;
    }

    // Recurse into any possible child elements.
    return _field_embed_element_children_has_format($child, $field_name);
  }

  // Base case: return FALSE.
  return FALSE;
}

/**
 * Helper function to recursively check the values against a field name.
 *
 * @param array $value
 *   A Form API $form_state value.
 * @param string $field_name
 *   The name of the field to check against.
 *
 * @return bool
 *   Returns TRUE if the $value has a reference to $field_name, or FALSE if it
 *   does not.
 */
function _field_embed_value_check_field(array $value, $field_name) {
  if (is_array($value)) {
    foreach ($value as $child) {
      if (!is_array($child)) {
        // There's no further possible nesting, so we should look through all of
        // the columns now attached to the field value.
        foreach ($value as $content) {
          // Perform the filtering.
          preg_match_all(FIELD_EMBED_FILTER_EXPRESSION, $content, $matches);

          // $matches[1] in this case has field names.
          foreach ($matches[1] as $match) {
            if ($match == $field_name) {
              // Return TRUE here because we have a match on the field name.
              return TRUE;
            }
          }
        }
      }

      // Recurse into any possible children.
      if (is_array($child)) {
        return _field_embed_value_check_field($child, $field_name);
      }
    }
  }

  // Base case: return FALSE.
  return FALSE;
}
