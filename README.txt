INTRODUCTION
------------
The Field Embed module provides a filter that allows any field on a page to be
embedded into another field.

INSTALLATION
------------
1. Unpack the field_embed folder and contents in the appropriate modules
   directory of your Drupal installation.

2. Enable the field_embed module on the Modules admin page.

CONFIGURATION
-------------
1. Configure your sites' text formats (admin/config/content/formats) to
   use the 'Field embed' filter. Usually you'll want to place the Field Embed
   filter at the bottom of your filter stack to ensure proper inclusion of
   <div> tags.

2. Create or edit any fieldable entity.

3. Select the appropriate text format as configured above in the field you
   would like to embed.

4. By using the syntax [[entity_name=entity_id:(field machine name):(field view mode)]]],
   this filter will embed the field with a given field machine name in an
   entity, with an optional field formatter.

TROUBLESHOOTING
---------------
Q: Why doesn't my filter render?

A: If you are using a WYSIWYG editor, make sure there are no HTML elements
   around the filter items.  The elements can be removed by viewing the source.
